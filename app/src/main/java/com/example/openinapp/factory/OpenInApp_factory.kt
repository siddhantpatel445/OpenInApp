package com.example.openinapp.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.openinapp.repository.OpenInAppRepository
import com.example.openinapp.viewmodel.OpenInAppViewModel

class OpenInApp_factory(private val openInAppRepository: OpenInAppRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OpenInAppViewModel::class.java)){
            return OpenInAppViewModel(openInAppRepository) as T
        }
        throw IllegalArgumentException("Unknown class")
    }
}