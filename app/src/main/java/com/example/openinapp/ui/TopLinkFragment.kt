package com.example.openinapp.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.openinapp.Adapter.TopLinksLAdapter
import com.example.openinapp.R
import com.example.openinapp.databinding.FragmentTopLinkBinding
import com.example.openinapp.factory.OpenInApp_factory
import com.example.openinapp.repository.OpenInAppRepository
import com.example.openinapp.viewmodel.OpenInAppViewModel


class TopLinkFragment : Fragment(R.layout.fragment_top_link) {
    lateinit var binding1: FragmentTopLinkBinding
    lateinit var factory: OpenInApp_factory
    var Positions: Int = 0
    lateinit var viewModel: OpenInAppViewModel
    lateinit var adapternew: TopLinksLAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding1 = FragmentTopLinkBinding.bind(view)
        factory = OpenInApp_factory(OpenInAppRepository())

        viewModel = ViewModelProvider(this, factory)[OpenInAppViewModel::class.java]

        binding1.lifecycleOwner = this

        binding1.topLinkRecyclerview.layoutManager = LinearLayoutManager(requireContext())

//        binding.personRecyclerview.layoutManager = GridLayoutManager(requireContext(), 2)
//        //binding.personRecyclerview.layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)

        viewModel.getdataitems().observe(viewLifecycleOwner) {
            adapternew = TopLinksLAdapter(it, requireContext())
            binding1.topLinkRecyclerview.adapter = adapternew
            adapternew.notifyDataSetChanged()
        }
    }

}