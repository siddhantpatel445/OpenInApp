package com.example.openinapp.Adapter

import android.content.Context
import android.provider.ContactsContract.Data
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.openinapp.R
import com.example.openinapp.Response.OpenApiResponse
import com.example.openinapp.databinding.ApiresponselayoutBinding

class TopLinksLAdapter(private val data: List<OpenApiResponse>,
                       private var context: Context
) : RecyclerView.Adapter<TopLinksLAdapter.AllDataViewHolder>()  {

    inner class AllDataViewHolder(var binding:ApiresponselayoutBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AllDataViewHolder {
        var view: View? = LayoutInflater.from(parent.context)
            .inflate(R.layout.apiresponselayout, parent, false)
        var binding: ApiresponselayoutBinding = DataBindingUtil.bind(view!!)!!
        return AllDataViewHolder(binding)
    }

    override fun getItemCount(): Int {

        return data.size

    }

    override fun onBindViewHolder(holder: AllDataViewHolder, position: Int) {
        val items = data[position]

        holder.binding.apply {
            //this.topLink.text=items.top_links.toString()
        }
    }

}