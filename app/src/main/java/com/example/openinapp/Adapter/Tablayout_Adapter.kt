package com.example.openinapp.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.openinapp.ui.TopLinkFragment
import com.example.openinapp.ui.TopLinks_RecenLink_Fragment.Recent_Links_Fragment
import com.example.openinapp.ui.TopLinks_RecenLink_Fragment.TopLinksFragment

const val TAB_COUNT = 2

class Tablayout_Adapter(private val fragment: FragmentActivity) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int {
        return TAB_COUNT
    }

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = TopLinkFragment()
            1 -> fragment = Recent_Links_Fragment()

        }
        return fragment!!
    }
}