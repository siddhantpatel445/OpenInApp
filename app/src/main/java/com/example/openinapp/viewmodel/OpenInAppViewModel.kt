package com.example.openinapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.openinapp.Response.OpenApiResponse
import com.example.openinapp.repository.OpenInAppRepository
import com.example.openinapp.ui.Model.Data

class OpenInAppViewModel(private val openInAppRepository: OpenInAppRepository): ViewModel() {
    fun getdataitems(): LiveData<List<OpenApiResponse>> {
        return openInAppRepository.getdataitemsList()
    }

}
