package com.example.openinapp.Response

import com.example.openinapp.ui.Model.Data

data class OpenApiResponse(
    var status: Boolean = false,
    var statusCode: Int = 0,
    var message: String = "",
    var support_whatsapp_number: String = "",
    var extra_income: Double = 0.0,
    var total_links: Int = 0,
    var total_clicks: Int = 0,
    var today_clicks: Int = 0,
    var top_source: String = "",
    var top_location: String = "",
    var startTime: String = "",
    var links_created_today: Int = 0,
    var applied_campaign: Int = 0,
    var data: Data

)
