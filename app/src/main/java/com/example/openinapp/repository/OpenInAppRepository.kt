package com.example.openinapp.repository

import android.content.ContentValues
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.openinapp.OpenInAppApi.openinappclient
import com.example.openinapp.Response.OpenApiResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class OpenInAppRepository {
    private var AlldataList: MutableLiveData<List<OpenApiResponse>> = MutableLiveData()
    fun getdataitemsList(): MutableLiveData<List<OpenApiResponse>> {
        val call = openinappclient.apiinterface.getdata()
        call.enqueue(object : Callback<OpenApiResponse> {
            override fun onResponse(
                call: Call<OpenApiResponse>,
                response: Response<OpenApiResponse>
            ) {
                if (response.isSuccessful) {
                    var body = response.body()
                    //    AlldataList.postValue(body?.datalist)
                    Log.d(ContentValues.TAG, "onResponse: $body")
                }
            }
            override fun onFailure(call: Call<OpenApiResponse>, t: Throwable) {
                Log.d(ContentValues.TAG, "on Failure ${t.message} ")
            }
        })

        return AlldataList
    }
}

