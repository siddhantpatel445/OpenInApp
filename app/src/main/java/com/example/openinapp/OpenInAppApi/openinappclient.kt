package com.example.openinapp.OpenInAppApi

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


private const val baseurl = "https://api.inopenapp.com/api/v1/"
object openinappclient {
    val retofitClient: Retrofit.Builder by lazy {
        val token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjU5MjcsImlhdCI6MTY3NDU1MDQ1MH0.dCkW0ox8tbjJA2GgUx2UEwNlbTZ7Rr38PVFJevYcXFI"

        val client = OkHttpClient.Builder().addInterceptor { chain ->
            val newRequest: Request = chain.request().newBuilder()
                .addHeader("Authorization", " Bearer $token")
                .build()
            chain.proceed(newRequest)
        }.build()

        Retrofit.Builder()
            .client(client)
            .baseUrl(baseurl)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
    }
    val apiinterface:openinappApi by lazy {
        retofitClient.build().create(openinappApi::class.java)
    }

}
