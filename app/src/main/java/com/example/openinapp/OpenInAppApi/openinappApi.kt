package com.example.openinapp.OpenInAppApi

import com.example.openinapp.Response.OpenApiResponse
import retrofit2.Call
import retrofit2.http.GET

interface openinappApi {
    @GET("dashboardNew")
    fun getdata(): Call<OpenApiResponse>
}